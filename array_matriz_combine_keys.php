<?php

$array1 = [1, 2];
$array2 = [77, 146];

const KEY_1 = 'key_1';
const KEY_2 = 'key_2';

$fp = [[1, 77], [1, 146], [2, 77], [2, 146]];
$fp2 = [[
    KEY_1 => 1,
    KEY_2 => 77,
], [
    KEY_1 => 1,
    KEY_2 => 146,
], [
    KEY_1 => 2,
    KEY_2 => 77,
], [
    KEY_1 => 2,
    KEY_2 => 146,
]];

$keys = [KEY_1, KEY_2];

/**
 * Gera uma matriz com todos os elementos das duas listas de itens e, opcionalmente,
 * pode atribuir chaves para ambas colunas criadas.
 *
 * @param array $array1
 * @param array $array2
 * @param array|null $keys
 * @return array
 */
function array_matriz_combine_keys(array $array1, array $array2, ?array $keys = []): array
{
    if ($keys && count($keys) != 2) {
        return ['message' => 'Enter two keys for generate matriz, please!'];
    }

    $results = [];

    foreach ($array1 as $a1) {
        foreach ($array2 as $a2) {
            if ($keys) {
                $results[] = array_combine($keys, [$a1, $a2]);
                continue;
            }

            $results[] = [$a1, $a2];
        }
    }

    return $results;
}

function assertEquals($param1, $param2): void
{
    if ($param1 == $param2) {
        echo "Passou Teste\n";
    } else {
        echo "Opa, não passou o Teste\n";
    }
}

function assertEmpty($param1): void
{
    if (!$param1) {
        echo "Passou Teste\n";
    } else {
        echo "Opa, não passou o Teste\n";
    }
}

$result = array_matriz_combine_keys($array1, $array2);
assertEquals($fp, $result);

$result2 = array_matriz_combine_keys($array1, $array2, $keys);
assertEquals($fp2, $result2);

$result3 = array_matriz_combine_keys($array1, $array2, ['a', 'b', 'c']);
assertEquals('Enter two keys for generate matriz, please!', $result3['message']);

$result4 = array_matriz_combine_keys([], [], $keys);
assertEmpty($result4);

$result5 = array_matriz_combine_keys([], []);
assertEmpty($result5);
